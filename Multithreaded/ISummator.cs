﻿
namespace Multithreaded
{
    internal interface ISummator
    {
        int ReturnSimpleSum(int[] arr);
        int ReturnParallelLinqSum(int[] arr);
        int ReturnParallel(int[] arr);
    }
}
