﻿
namespace Multithreaded
{
    internal class Summator:ISummator
    {
        private static readonly object _lock = new();
        public int ReturnSimpleSum(int[] arr)
        {
            int sum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                sum+=arr[i];
            }
            return sum;
        }
        public int ReturnParallelLinqSum(int[] arr) => arr.AsParallel().Sum();

        public int ReturnParallel(int[] arr)
        {
            int threadCount = 4;

            int resWithoutRem = arr.Length / threadCount;
            int rem = GetRemainder(arr.Length, threadCount);

            var threads = new List<Thread>();
            int sum = 0;

            for (int i = 0; i < threadCount - rem; i++)
            {
                int start = i * resWithoutRem;
                int stop = (i + 1) * resWithoutRem;

                var t = new Thread(() => Sum(arr, start, stop, ref sum));
                t.Start();
                threads.Add(t);
            }

            if (rem != 0)
            {
                int start = (threadCount - 1) * resWithoutRem;
                int stop = arr.Length;

                var t = new Thread(() => Sum(arr, start, stop, ref sum));
                t.Start();
                threads.Add(t);
            }

            foreach (var t in threads)
            {
                t.Join();
            }

            return sum;
        }
        private static void Sum(int[] arr, int start, int stop, ref int fullSum)
        {
            lock (_lock)
            {
                int sum = 0;

                for (int i = start; i < stop; i++)
                {
                    sum += arr[i];
                }

                fullSum += sum;
            }
        }
        private static int GetRemainder(int lenght, int threadCount)
        {
            return lenght % threadCount == 0 ? 0 : 1;
        }
    }
}
