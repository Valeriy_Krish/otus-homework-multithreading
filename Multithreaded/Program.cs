﻿using System.Diagnostics;

namespace Multithreaded
{
    public class Program
    {
        static void Main()
        {
            var timer = new Stopwatch();
            var sum = new Summator();
            var item = new List<int>()
                { 100000, 1000000, 10000000 };

            foreach (var result in item)
            {
                RandomArr(result, out int[] arr);

                timer.Start();
                var sumSimple = sum.ReturnSimpleSum(arr);
                Console.WriteLine($"Затраченное время на вычисления для {result} элементов (последовательная) :{GetTimeFormat(timer.Elapsed)}");

                timer.Restart();
                var sumParallelLinq = sum.ReturnParallelLinqSum(arr);
                Console.WriteLine($"Затраченное время на вычисления для {result} элементов (AsParallel) :{GetTimeFormat(timer.Elapsed)}");

                timer.Restart();
                var sumParallel = sum.ReturnParallel(arr);
                Console.WriteLine($"Затраченное время на вычисления для {result} элементов (Thread) :{GetTimeFormat(timer.Elapsed)}");

                timer.Stop();
            }
        }
        private static void RandomArr (int count , out int[] arr)
        {
            var rand = new Random();
            arr = new int[count];

            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = rand.Next(5);
            }
        }
        private static string GetTimeFormat(TimeSpan time)
        {
            return string.Format("{0:00}.{1:00}", time.Seconds, time.Milliseconds);
        }
    }
}
